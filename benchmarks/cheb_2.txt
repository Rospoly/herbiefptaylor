Variables
  float64 x in [-1, 1];

Definitions
  T0 rnd32= 1;
  T1 rnd32= x;
  T2 rnd32= 2 * x * T1 - T0;

Expressions
  cheb2 = T2;

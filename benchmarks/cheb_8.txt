Variables
  float64 x in [-1, 1];

Definitions
  T0 rnd32= 1;
  T1 rnd32= x;
  T2 rnd32= 2 * x * T1 - T0;
  T3 rnd32= 2 * x * T2 - T1;
  T4 rnd32= 2 * x * T3 - T2;
  T5 rnd32= 2 * x * T4 - T3;
  T6 rnd32= 2 * x * T5 - T4;
  T7 rnd32= 2 * x * T6 - T5;
  T8 rnd32= 2 * x * T7 - T6;
  
Expressions
  cheb8 = T8;

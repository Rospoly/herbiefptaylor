Variables
  float64 x in [-1, 1];

Definitions
  P0 rnd32= 1;
  P1 rnd32= x;
  P2 rnd32= (3 * x * P1 - P0) / 2;
  P3 rnd32= (5 * x * P2 - 2 * P1) / 3;
  P4 rnd32= (7 * x * P3 - 3 * P2) / 4;
  P5 rnd32= (9 * x * P4 - 4 * P3) / 5;
  P6 rnd32= (11 * x * P5 - 5 * P4) / 6;
  P7 rnd32= (13 * x * P6 - 6 * P5) / 7;

Expressions
  legendre7 = P7;
